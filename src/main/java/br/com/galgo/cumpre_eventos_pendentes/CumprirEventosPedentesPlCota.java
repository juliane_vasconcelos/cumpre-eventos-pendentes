package br.com.galgo.cumpre_eventos_pendentes;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.galgo.relatorio.GerarRelatorio;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;

public class CumprirEventosPedentesPlCota extends CumprirEventosPedentes {

	private static final Servico servico = Servico.PL_COTA;

	@BeforeClass
	public static void setUp() throws Exception {
		GerarRelatorio.zerarContadores();
		GerarRelatorio.gerarPrintInicial();
	}

	@Test
	public void cumprirExtrato() throws ErroAplicacao {
		cumprirEventos(servico, Ambiente.HOMOLOGACAO);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		GerarRelatorio.gerarPrintFinal();
		// GerarRelatorio.gerarRelatorio(servico);
	}

}
