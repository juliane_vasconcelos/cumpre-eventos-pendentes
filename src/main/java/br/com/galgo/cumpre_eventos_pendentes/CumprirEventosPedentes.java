package br.com.galgo.cumpre_eventos_pendentes;

import java.util.List;

import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.compromisso.TelaPainelCompromissos;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvio;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class CumprirEventosPedentes extends TelaGalgo {

	private final String caminhoUsuarios = "K:/Condominio_STI/001 - Sist Galgo/Documentacao/04 Testes/Usuários/Usuários Eventos.xlsx";
	private final String painelCompGeral = "eventosPendentes.png";
	private final String erroValor = "erroValorPlCotaDiferentePosicao.png";
	private final String painelCompCumpridos = "eventosCumpridos.png";

	public void cumprirEventos(Servico servico, Ambiente ambiente)
			throws ErroAplicacao {
		List<Usuario> listaUsuarios = ArquivoUtils.getUsuarioCumprirEvento(
				ambiente, caminhoUsuarios, servico);

		for (Usuario usuario : listaUsuarios) {
			cumprirEventosPendentes(usuario, servico);
		}
	}

	public void cumprirEventosPendentes(Usuario usuario, Servico servico)
			throws ErroAplicacao {
		int totalEventosASeremCumpridos = 0;
		int totalEventosCumpridos = 0;

		TelaGalgo.abrirBrowser(usuario.getAmbiente().getUrl());
		TelaLogin telaLogin = new TelaLogin();
		TelaHome telaHome = telaLogin.loginAs(usuario);

		TelaPainelCompromissos telaPainelCompromissos = (TelaPainelCompromissos) telaHome
				.acessarSubMenu(SubMenu.PAINEL_COMPROMISSO);

		esperarSegundos(1);
		telaPainelCompromissos.clicarFiltrosDeBusca();
		esperarSegundos(1);
		telaPainelCompromissos.preecherServico(servico);
		telaPainelCompromissos.clicarBotaoConfirmar();

		TesteUtils.tirarPrint(painelCompGeral);

		if (telaPainelCompromissos.clicarVencidosDiasAnteriores()) {
			telaPainelCompromissos.clicarGrafico();

			totalEventosASeremCumpridos = telaPainelCompromissos
					.getQtdEventosASeremCumpridos();
			System.out.println(totalEventosASeremCumpridos);

			boolean continuarEnviando = true;
			TelaEnvio telaEnvioDeInformacao = null;
			int posicao = 1;

			while (continuarEnviando) {
				try {

					String dataBase = telaPainelCompromissos
							.getDataBase(posicao);
					String codigoSTI = null;
					String cotista = null;

					if (servico == Servico.POSICAO_ATIVOS) {
						codigoSTI = telaPainelCompromissos.getCodSTI();
					}

					telaEnvioDeInformacao = telaPainelCompromissos
							.clicarBotaoEnviar(servico, usuario, codigoSTI,
									dataBase, cotista, posicao);
					telaEnvioDeInformacao.enviar();
					if (verificaTextoNaTela("A(s) seguinte(s) informação(ões) está(ão) divergente(s) com a(s) registrada(s) no Serviço")) {
						TesteUtils.tirarPrint(erroValor);
						telaEnvioDeInformacao.clicarBotaoVoltar();
						fecharAlertaBrowser();
						posicao++;
					} else {
						telaEnvioDeInformacao.clicarBotaoVoltar();
					}
					totalEventosCumpridos++;
					if (totalEventosCumpridos == totalEventosASeremCumpridos) {
						continuarEnviando = false;
					}
				} catch (Exception e) {
					continuarEnviando = false;

					if (!verificaTextoNaTela("Não foram encontrados")) {
						TesteUtils.tirarPrint(painelCompCumpridos);
						if (totalEventosCumpridos < totalEventosASeremCumpridos) {
							System.out
									.println("Terminou sem cumprir todos os eventos! Verificar o print.\n Eventos cumpridos ="
											+ totalEventosCumpridos
											+ ". \n Total de eventos a serem cumpridos="
											+ totalEventosASeremCumpridos + ".");
						}
					}
				}
			}
			telaLogin.logout();
			TelaGalgo.fecharBrowser();
		} else {
			telaLogin.logout();
			TelaGalgo.fecharBrowser();
		}
	}
}
