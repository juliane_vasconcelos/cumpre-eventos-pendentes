package br.com.galgo.cumpre_eventos_pendentes;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.galgo.relatorio.GerarRelatorio;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;

public class CumprirEventosPedentesInformacaoAnbima extends
		CumprirEventosPedentes {

	public static final Servico servico = Servico.INFO_ANBIMA;

	@BeforeClass
	public static void setUp() throws Exception {
		GerarRelatorio.zerarContadores();
		GerarRelatorio.gerarPrintInicial();
	}

	@Test
	public void cumprirInfoAnbima() throws ErroAplicacao {
		cumprirEventos(Servico.INFO_ANBIMA, Ambiente.HOMOLOGACAO);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		GerarRelatorio.gerarPrintFinal();
		// GerarRelatorio.gerarRelatorio(servico);
	}
}
