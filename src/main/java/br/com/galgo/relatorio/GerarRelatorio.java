package br.com.galgo.relatorio;

import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.compromisso.TelaPainelCompromissos;
import br.com.galgo.testes.recursos_comuns.teste.TesteConsultaCompromisso;
import br.com.galgo.testes.recursos_comuns.utils.DocUtils;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class GerarRelatorio extends TelaGalgo {

	public static int qtdDeEventosPendentesPre = 0;
	public static int qtdDeEventosPendentesPos = 0;
	public static final String PATH_ANTES = "cumpre-eventos-inicio.png";
	public static final String PATH_DEPOIS = "cumpre-eventos-fim.png";
	public static final String PATH_RELATORIO_TEMPLATE = "eventos/Sistema Galgo - Cumprimento de Eventos - Homologação- Template.docx";
	public static final String PATH_RELATORIO = "Sistema Galgo - Cumprimento de Eventos - Homologação";

	public static void zerarContadores() throws Exception {
		qtdDeEventosPendentesPre = 0;
		qtdDeEventosPendentesPos = 0;
	}

	public static void gerarPrintInicial() throws Exception {
		TesteConsultaCompromisso testeConsultaCompromisso = new TesteConsultaCompromisso();
		testeConsultaCompromisso.configura(Ambiente.HOMOLOGACAO,
				UsuarioConfig.USUARIO_FINAL_STI_HOMOL);
		testeConsultaCompromisso.setUp();
		testeConsultaCompromisso.testeConsultaPainelCompromissos();
		TelaPainelCompromissos telaPainelCompromissos = new TelaPainelCompromissos();

		qtdDeEventosPendentesPre = telaPainelCompromissos
				.pegarQtdDeEventosPendentes();
		TesteUtils.finalizarTeste(PATH_ANTES);
	}

	public static void gerarPrintFinal() throws Exception {
		TesteConsultaCompromisso testeConsultaCompromisso = new TesteConsultaCompromisso();
		testeConsultaCompromisso.configura(Ambiente.HOMOLOGACAO,
				UsuarioConfig.USUARIO_FINAL_STI_HOMOL);
		testeConsultaCompromisso.setUp();
		testeConsultaCompromisso.testeConsultaPainelCompromissos();
		TelaPainelCompromissos telaPainelCompromissos = new TelaPainelCompromissos();

		qtdDeEventosPendentesPos = telaPainelCompromissos
				.pegarQtdDeEventosPendentes();

		TesteUtils.finalizarTeste(PATH_DEPOIS);
	}

	public static void gerarRelatorio(Servico servico) throws Exception {
		int porcentagem = 1 - (qtdDeEventosPendentesPos / qtdDeEventosPendentesPre);

		final String relatorioPath = PATH_RELATORIO +" - "+ servico.getDesc()
				+ ".docx";
		
		DocUtils.gerarRelatorioCumprirEventosHomologacao(
				PATH_RELATORIO_TEMPLATE, relatorioPath, qtdDeEventosPendentesPre,
				qtdDeEventosPendentesPos, porcentagem, PATH_ANTES, PATH_DEPOIS);
	}
}
